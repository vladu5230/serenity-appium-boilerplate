# Serenity Appium Boilerplate

BDD Framework for Mobile and API Automated Testing.

Stack:

- [ ] [Serenity Framework](https://serenity-bdd.info/)
- [ ] [Appium](https://appium.io/)
- [ ] [Cucumber](https://cucumber.io/)
- [ ] [REST Assured](https://rest-assured.io/)

## Framework features


### Project structure
- features (.feature file definitions, found in ***src/test/resources/features***)
  - keywords (implementations of keywords from the feature files, found in ***src/test/java/com/tremend/serenity/keywords***)
    - steps (implementation of steps used in keywords, found in ***src/test/java/com/tremend/serenity/steps***)
      - pages (page objects storing locators and functions to interact with or make assertions on specific pages, found in ***src/test/java/com/tremend/serenity/pages***)

- runner (properties for the Cucumber runner found in ***src/test/java/com/tremend/serenity/_runner/CucumberTestSuite.java***)
- config (various classes for Appium and REST Assured config, found in ***src/test/java/com/tremend/serenity/config***)
- utils (various tools used for dealing with properties, common methods, Java Object DTOs, enums, etc., found in ***src/test/java/com/tremend/serenity/utils***)

Information regarding device and base URL configuration found in the *Properties* section of this readme file.

## Installation

### Minimum requirements:

- [ ] Install [**Java 11 JDK**](https://adoptopenjdk.net/)
- [ ] Install [**Maven**](https://maven.apache.org/install.html)
- [ ] Install [**Node.js**](https://nodejs.org/en/download/)
- [ ] Install **Appium** using NPM from the Terminal:
```
$ npm install -g appium
```
- [ ] Install [**Androd Studio**](https://developer.android.com/studio)
- [ ] Add the following paths to the PATH variable: [$JAVA_HOME](https://www.java.com/en/download/help/path.html), $ANDROID_HOME, $ANDROID_HOME/emulator, $ANDROID_HOME/tools, $ANDRID_HOME/platform-tools, $APPIUM_HOME, $NODE_HOME
- [ ] Test that each installed tool is linked to the environment variable PATH by running commands in the terminal:
```
$ java --version
$ mvn --version
$ node --version
$ adb --version
$ appium --version
```
- [ ] Install **appium-doctor**:
```
$ npm install -g appium-doctor
```
- [ ] [Optional] Install [**XCode**](https://apps.apple.com/us/app/xcode/id497799835?mt=12) (only available on macOS
  machines)

- [ ] Run appium-doctor to verify setup is completed:
```
$ appium-doctor --android

$ appium-doctor --iOS #(if macOS machine is available)
```

- [ ] Install **Maven project dependencies** by running:
```
$ mvn install
```

### Local development tools

- [ ] Install [Appium GUI desktop app](https://github.com/appium/appium-desktop/releases/tag/v1.22.3) for starting the
  Appium server locally
- [ ] Install [Appium Inspector desktop app](https://github.com/appium/appium-inspector/releases) for identifying
  locators

## Getting ready for local development and running tests

1. **Start the Appium server:**

- [ ] Open the Appium Server GUI Application
- [ ] Click on **Edit configurations** and add your ANDROID_HOME and JAVA_HOME paths
- [ ] Save and restart
- [ ] Go to the **Advanced tab** and make sure the **Allow CORS** and **Relaxed Security** checkboxes are selected
- [ ] Click **Start server** using the default Host and Port values

2. **Start Androd studio and start an emulator**

- [ ] [Create a Android Virtual Device](https://developer.android.com/studio/run/managing-avds#createavd) - Pixel_5_API_32 is used in this framework
- [ ] Run that device from Android Studio or by running a terminal command:
```
$ emulator -avd Pixel_5_API_32
```
- [ ] Run the following command in the Terminal to find the device ID
```
$ adb devices
```
- [ ] Verify that the device ID is the same as the one found in the corresponding ***src/test/resources/app/appium_properties*** and ***src/test/resources/app/appium_inspector_config*** files

3. [Optional] **Start XCode Simulator**

- [ ] Launch XCode
- [ ] Navigate to XCode > Open Developer Tool > Simulator
- [ ] Choose a device
- [ ] Run the following command in the Terminal to find the device ID
```
$ xcrun xctrace list devices
```
- [ ] Once installed, you can also run the simulator using a Terminal command:
```
$ open -a Simulator
```

4. **Start the Appium Inspector** (make sure to have the Appium server started first)

- [ ] Open the **Appium Inspector Application**
- [ ] Leave **Host** and **Port** values as default
- [ ] Set **Remote Path** to:
```
/wd/hub
```

- [ ] Edit the **JSON Representation** field with the contents of a json config file found in ***
  src/test/resources/appium_inspector_config***
- [ ] **Verify that the UUIDs from the config file are correct** after executing steps 2 and/or 3
- [ ] Click **Start Session**
- [ ] For debugging check the Appium Server console

### Properties

- [ ] For API testing, configure base URLs for each specific environment of the application under test in ***src/test/resources/serenity.conf***.

- [ ] For Mobile testing, the device for running tests is indicated by the following property in the ***serenity.properties*** file:
```
device=androidEmulator  (or iosEmulator, if avalable)
``` 
Property files for Appium configuration are found in ***src/test/resources/app/appium_properties***. Supported devices are listed in ***src/test/java/com/tremend/serenity/utils/properties/SupportedDevices.java***


### API testing

This framework uses REST Assured library for testing APIs, wrapped in MicroServiceRequest.java. An example of using this wrapper and passing the Bearer Token for an API test has been added in the project.

### How to run the tests

Tests can be run in the IDEA from the CucumberTestSuite.java runner or from the feature files, using IntelliJ IDEA's Cucumber for Java plugin.

For running the tests in CLI using maven:

#### Run Mobile Tests and filter by Cucumber Tags:

    smoke       $ mvn clean verify -Ddevice=androidEmulator -Dcucumber.filter.tags='@mobile and @android and @smoke'
    sanity      $ mvn clean verify -Ddevice=androidEmulator -Dcucumber.filter.tags='@mobile and @android and (@smoke or @sanity)'
    regression  $ mvn clean verify -Ddevice=iosEmulator -Dcucumber.filter.tags='@mobile and @ios'

#### Run API Tests and filter by Cucumber Tags:

    smoke       $ mvn clean verify -Denvironment=qa -Dcucumber.filter.tags='@api and @smoke'
    sanity      $ mvn clean verify -Denvironment=qa -Dcucumber.filter.tags='@api and (@smoke or @sanity)'
    regression  $ mvn clean verify -Denvironment=qa -Denvironment=qa -Dcucumber.filter.tags='@api'

#### Serenity test report path:

   ***target/site/serenity/index.html***

### Code convention

Only step_methods should be named with snake case (e.g. stepsClass.method_with_snake_case), the rest of them should be
named with camel case (e.g. pageClass.methodWithCamelCase)

### IntelliJ IDEA Plugins

For the best development experience, install the following IntelliJ IDEA Plugins:

- [ ] Lombok
- [ ] Cucumber for Java
