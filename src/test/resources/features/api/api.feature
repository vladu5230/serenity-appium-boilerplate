Feature: API Tests

  @smoke @api
  Scenario: As a user I can get booking details through an API
    Given that the user is authenticated through an API
    When the user makes a POST request to create a booking
    Then the user finds that the booking was created correctly