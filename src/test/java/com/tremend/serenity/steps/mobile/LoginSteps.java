package com.tremend.serenity.steps.mobile;

import com.tremend.serenity.pages.mobile.LoginPage;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Step;

@Slf4j
public class LoginSteps {

    private LoginPage loginPage;

    @Step
    public void login_with_username_and_password(String username, String password){
        loginPage.loginWithUsernameAndPassword(username, password);
    }
}
