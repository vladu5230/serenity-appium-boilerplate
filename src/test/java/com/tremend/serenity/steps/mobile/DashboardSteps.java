package com.tremend.serenity.steps.mobile;


import com.tremend.serenity.pages.mobile.DashboardPage;
import net.thucydides.core.annotations.Step;
import org.testng.Assert;

public class DashboardSteps {

    DashboardPage dashboardPage;

    @Step
    public void assert_dashboard_page_is_loaded() {
        dashboardPage.waitUntilDashboardPageIsLoaded();
        Assert.assertEquals(dashboardPage.getPageSubtitle(), "PRODUCTS", "Dashboard title");
        Assert.assertTrue(dashboardPage.getVisibilityOfMenuButton(), "Menu button");
        Assert.assertTrue(dashboardPage.getVisibilityOfShoppingCart(), "Shopping cart button");
    }

}
