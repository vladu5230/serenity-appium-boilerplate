package com.tremend.serenity.steps.api;

import com.tremend.serenity.config.api.config.HttpMethod;
import com.tremend.serenity.config.api.config.MicroserviceRequest;
import com.tremend.serenity.utils.constants.framework.TestConstant;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import org.testng.Assert;

@Slf4j
public class AuthApiSteps {

    @Step
    public void login_api(String username, String password) {
        MicroserviceRequest login = new MicroserviceRequest();

        login.addToBasePath("auth");
        login.addBody(String.format(("{\"username\" : \"%s\", \"password\" : \"%s\"}"), username, password));

        Response response = login.executeRequestAndExtractResponse(HttpMethod.POST);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.SC_OK);

        TestConstant.AUTH_TOKEN = response.path("token");
    }

}
