package com.tremend.serenity.steps.api;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.tremend.serenity.config.api.config.HttpMethod;
import com.tremend.serenity.config.api.config.MicroserviceEnum;
import com.tremend.serenity.config.api.config.MicroserviceRequest;
import com.tremend.serenity.utils.api.models.BookingDatesDto;
import com.tremend.serenity.utils.api.models.BookingDto;
import com.tremend.serenity.utils.constants.sessionvars.SessionVars;
import com.tremend.serenity.utils.misc.DateUtil;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import org.testng.Assert;


@Slf4j
public class BookingApiSteps {

    private Faker faker = new Faker();
    private JsonParser jsonParser = new JsonParser();

    @Step
    public void create_booking_request_body() {
        BookingDto body = new BookingDto();

        body.setFirstname(faker.name().firstName());
        body.setLastname(faker.name().lastName());
        body.setTotalprice(faker.number().numberBetween(100, 500));
        body.setDepositpaid(faker.bool().bool());
        body.setAdditionalneeds(faker.lorem().word());

        BookingDatesDto dates = new BookingDatesDto();
        dates.setCheckin("2022-07-14");
        dates.setCheckout("202-07-08");
        body.setBookingdates(dates);

        Serenity.setSessionVariable(SessionVars.BOOKING_REQUEST_BODY).to(body);
    }

    @Step
    public void make_POST_request_for_create_booking(BookingDto body) {
        MicroserviceRequest getBookingIds = new MicroserviceRequest(MicroserviceEnum.BOOKING);
        getBookingIds.addBody(new Gson().toJson(body));

        Response response = getBookingIds.executeRequestAndExtractResponse(HttpMethod.POST);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.SC_OK);

        Serenity.setSessionVariable(SessionVars.BOOKING_RESPONSE_BODY).to(response);
    }

    @Step
    public void assert_booking_was_created_correctly() {
        BookingDto expected = Serenity.sessionVariableCalled(SessionVars.BOOKING_REQUEST_BODY);
        Response response = Serenity.sessionVariableCalled(SessionVars.BOOKING_RESPONSE_BODY);

        BookingDto actual = new Gson().fromJson(jsonParser.parse(response.path("booking").toString()), BookingDto.class);

        Assert.assertNotNull(response.path("bookingid"));
        Assert.assertEquals(actual.getFirstname(), expected.getFirstname());
        Assert.assertEquals(actual.getLastname(), expected.getLastname());
        Assert.assertEquals(actual.getAdditionalneeds(), expected.getAdditionalneeds());
        Assert.assertEquals(actual.getDepositpaid(), expected.getDepositpaid());
        Assert.assertEquals(actual.getTotalprice(), expected.getTotalprice());
        Assert.assertEquals(actual.getBookingdates().getCheckin(), expected.getBookingdates().getCheckin());
        Assert.assertEquals(actual.getBookingdates().getCheckout(), expected.getBookingdates().getCheckout());
        Assert.assertEquals(actual.getBookingdates().getAdditionalProperties(), expected.getBookingdates().getAdditionalProperties());
    }
}
