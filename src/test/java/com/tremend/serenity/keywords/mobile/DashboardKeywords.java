package com.tremend.serenity.keywords.mobile;

import com.tremend.serenity.pages.mobile.DashboardPage;
import com.tremend.serenity.steps.mobile.DashboardSteps;
import com.tremend.serenity.steps.mobile.LoginSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class DashboardKeywords {

    @Steps
    private DashboardSteps dashboardSteps;

    /**
     * ==================
     * GIVEN STEPS
     * ===================
     */


    /**
     * ==================
     * WHEN STEPS
     * ===================
     */


    /** ==================
     *   THEN STEPS
     * ===================
     */

    @Then("^the user finds that the dashboard page is loaded correctly$")
    public void user_logs_with_valid_credentials() {
        dashboardSteps.assert_dashboard_page_is_loaded();
    }

}
