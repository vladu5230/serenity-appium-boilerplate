package com.tremend.serenity.keywords.mobile;

import com.tremend.serenity.steps.api.AuthApiSteps;
import com.tremend.serenity.steps.mobile.LoginSteps;
import com.tremend.serenity.utils.constants.framework.TestConstant;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import static com.tremend.serenity.utils.constants.framework.TestConstant.USER;
import static com.tremend.serenity.utils.constants.framework.TestConstant.PASSWORD;

public class AccountAccessKeywords {

    @Steps
    private LoginSteps loginSteps;



    /**
     * ==================
     * GIVEN STEPS
     * ===================
     */


    /**
     * ==================
     * WHEN STEPS
     * ===================
     */

    @When("^the user logs with valid credentials$")
    public void user_logs_with_valid_credentials() {
        loginSteps.login_with_username_and_password(USER, PASSWORD);
    }

    @When("^the user logs with invalid credentials$")
    public void user_logs_with_invalid_credentials() {
        loginSteps.login_with_username_and_password("", "");
    }

    /**
     * ==================
     * THEN STEPS
     * ===================
     */
}
