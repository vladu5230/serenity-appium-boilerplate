package com.tremend.serenity.keywords.api;

import com.tremend.serenity.steps.api.AuthApiSteps;
import com.tremend.serenity.utils.constants.framework.TestConstant;
import io.cucumber.java.en.Given;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Steps;

@Slf4j
public class AuthApiKeywords {

    @Steps
    private AuthApiSteps authApiSteps;

    /**
     * ==================
     * GIVEN STEPS
     * ===================
     */

    @Given("that the user is authenticated through an API")
    public void api_login() {
        authApiSteps.login_api(TestConstant.API_USER, TestConstant.API_PASSWORD);
    }
}
