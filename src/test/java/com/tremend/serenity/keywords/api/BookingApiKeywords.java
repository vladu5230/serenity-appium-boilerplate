package com.tremend.serenity.keywords.api;

import com.tremend.serenity.steps.api.BookingApiSteps;
import com.tremend.serenity.utils.api.models.BookingDto;
import com.tremend.serenity.utils.constants.sessionvars.SessionVars;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class BookingApiKeywords {

    @Steps
    BookingApiSteps bookingApiSteps;

    /** ==================
     *   GIVEN STEPS
     * ===================
     */



    /** ==================
     *   WHEN STEPS
     * ===================
     */

    @When("the user makes a POST request to create a booking")
    public void make_POST_request_for_create_booking() {
        bookingApiSteps.create_booking_request_body();
        BookingDto body = Serenity.sessionVariableCalled(SessionVars.BOOKING_REQUEST_BODY);
        bookingApiSteps.make_POST_request_for_create_booking(body);
    }

    /** ==================
     *   Then STEPS
     * ===================
     */

    @Then("the user finds that the booking was created correctly")
    public void assert_booking_created() {
        bookingApiSteps.assert_booking_was_created_correctly();
    }
}
