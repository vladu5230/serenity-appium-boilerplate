package com.tremend.serenity.pages.mobile;

import com.tremend.serenity.utils.elements.MobileAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@Slf4j
public class LoginPage extends MobileAction {

    public LoginPage(WebDriver driver){
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"test-Username\"]")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name=\"test-Username\"]")
    private WebElement usernameField;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"test-Password\"]")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeSecureTextField[@name=\"test-Password\"]")
    private WebElement passwordField;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-LOGIN\"]")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"test-LOGIN\"]")
    private WebElement loginButton;


    //---------------------------------------- Methods ---------------------------------------- //

    public void loginWithUsernameAndPassword(String username, String password){
        sendKeys(usernameField, username);
        sendKeys(passwordField, password);
        click(loginButton);
    }
}
