package com.tremend.serenity.pages.mobile;

import com.tremend.serenity.utils.elements.MobileAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DashboardPage extends MobileAction {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Cart drop zone\"]/android.view.ViewGroup/android.widget.TextView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"PRODUCTS\"]")
    private WebElement pageSubtitle;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Cart\"]/android.view.ViewGroup/android.widget.ImageView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"test-Cart\"]/XCUIElementTypeOther")
    private WebElement shoppingCartButton;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Menu\"]/android.view.ViewGroup/android.widget.ImageView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"test-Menu\"]/XCUIElementTypeOther")
    private WebElement menuButton;


    public void waitUntilDashboardPageIsLoaded() {
        waitFor(pageSubtitle);
    }

    public String getPageSubtitle() {
        return pageSubtitle.getText();
    }

    public boolean getVisibilityOfShoppingCart() {
        return shoppingCartButton.isDisplayed();
    }

    public boolean getVisibilityOfMenuButton() {
        return menuButton.isDisplayed();
    }
}
