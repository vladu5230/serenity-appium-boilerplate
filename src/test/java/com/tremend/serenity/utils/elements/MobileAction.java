package com.tremend.serenity.utils.elements;


import com.google.common.collect.ImmutableMap;
import com.tremend.serenity.config.appium.MobilePageObject;
import com.tremend.serenity.utils.properties.PropertyHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.time.Duration;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static java.time.Duration.ofSeconds;


@Slf4j
public class MobileAction extends MobilePageObject {

    public MobileAction(WebDriver driver) {
        super(driver);
    }

    public boolean isIos = PropertyHelper.getMobileOSType().equals("ios");

    public AppiumDriver<MobileElement> driver = ((AppiumDriver) ((WebDriverFacade) getDriver()).getProxiedDriver());

    public AppiumDriver<MobileElement> getConnexionAppium() {
        return this.driver;
    }

    public void click(WebElement element) {
        int x = element.getLocation().getX() + (element.getSize().getWidth() / 2);
        int y = element.getLocation().getY() + (element.getSize().getHeight() / 2);
        new TouchAction(driver).tap(new TapOptions().withPosition(new PointOption<>().withCoordinates(x, y))).perform().release();
    }

    // Keyboard

    public void sendKeys(WebElement element, String text) {
        waitFor(element);
        element.clear();
        element.sendKeys(text);
    }

    public void sendKeys(WebElement element, Keys keys) {
        waitFor(element);
        element.clear();
        element.sendKeys(keys);
    }

    public void hideKeyboard() {
        try {
            driver.hideKeyboard();
        } catch (Exception e) {
            System.out.println("Error in hiding the keyboard");
        }
    }

    public void tapKeyboardSearchButton() {
        driver.executeScript("mobile: performEditorAction", ImmutableMap.of("action", "search"));
    }


    //Swipe actions

    public void swipeToBottom(int howManySwipes) {
        Dimension dim = driver.manage().window().getSize();
        int height = dim.getHeight();
        int width = dim.getWidth();
        int x = (int) (width / 2.1);
        int top_y = (int) (height * 0.80);
        int bottom_y = (int) (height * 0.21);
        for (int i = 1; i <= howManySwipes; i++) {
            System.out.println("coordinates :" + x + "  " + top_y + " " + bottom_y);
            TouchAction ts = new TouchAction(driver);
            ts.press(PointOption.point(x, top_y)).waitAction(waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, bottom_y)).release().perform();
            waitABit(500);
        }
    }

    public void shortSwipeVertical(int swipeTimes) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < swipeTimes; i++) {
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.7);
            int endPoint = (int) (size.height * 0.4);
            try {
                new TouchAction<>(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                log.error("" + wde);
                new TouchAction<>(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
        }

    }

    public void shortSwipeVerticalReverse() {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 2; i++) {
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.3);
            int endPoint = (int) (size.height * 0.6);
            try {
                new TouchAction<>(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                new TouchAction<>(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
        }
    }
}
