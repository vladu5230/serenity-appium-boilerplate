package com.tremend.serenity.utils.api.models;

import lombok.Data;

import java.util.Map;

@Data
public class BookingDatesDto {

    private String checkin;
    private String checkout;
    private Map<String, Object> additionalProperties;

}
