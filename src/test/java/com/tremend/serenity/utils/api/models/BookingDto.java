package com.tremend.serenity.utils.api.models;


import lombok.Data;

import java.util.Map;

@Data
public class BookingDto {

    private String firstname;
    private String lastname;
    private Integer totalprice;
    private Boolean depositpaid;
    private BookingDatesDto bookingdates;
    private String additionalneeds;
    private Map<String, Object> additionalProperties;

}
