package com.tremend.serenity.utils.properties;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

public class PropertyHelper {

    private PropertyHelper() {
        throw new IllegalStateException("Utility class");
    }


    public static String getSerenityProperty(String propertyName) {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return EnvironmentSpecificConfiguration.from(variables).getProperty(propertyName);
    }

    public static String getAppiumProperty(String propertyName) {
        final Properties properties = new Properties();
        InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("app/appium_properties/" + getMobileOSType().toLowerCase(Locale.ROOT) + ".properties");
        try {
            properties.load(inStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(propertyName);
    }

    public static String getWebDriverBaseUrl() {
        return getSerenityProperty("webdriver.base.url");
    }

    public static String getSystemEnvironmentVariable(String variable) {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return EnvironmentSpecificConfiguration.from(variables).getProperty(variable);
    }

    public static String getDriverTypeValue() {
        return getSerenityProperty("webddriver.driver");
    }

    public static String getEnvironment() {
        return getSerenityProperty("environment");
    }

    public static String getRunTags() {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return variables.getProperty("cucumber.filter.tags");
    }

    public static String getMobileOSType() {
        return getSerenityProperty("device");
    }

    public static String getRestAssuredTimeout() {
        return getSerenityProperty("rest.assured.timeout");
    }
}
