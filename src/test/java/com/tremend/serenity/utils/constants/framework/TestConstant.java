package com.tremend.serenity.utils.constants.framework;


public class TestConstant {

    private TestConstant() {
        throw new IllegalStateException("Utility class");
    }

    public static String AUTH_TOKEN                                            = "null AUTH_TOKEN";
    public static final String USER                                            = "standard_user";
    public static final String PASSWORD                                        = "secret_sauce";
    public static final String API_USER                                        = "admin";
    public static final String API_PASSWORD                                    = "password123";

}
