package com.tremend.serenity.utils.constants.framework;

import com.tremend.serenity.utils.properties.PropertyHelper;

public class MobileProperty {

    public static final String PLATFORM_VERSION = PropertyHelper.getAppiumProperty("appium.platformVersion");
    public static final String PLATFORM_NAME = PropertyHelper.getAppiumProperty("appium.platformName");
    public static final String DEVICE_NAME = PropertyHelper.getAppiumProperty("appium.deviceName");
    public static final String UDID = PropertyHelper.getAppiumProperty("appium.udid");
    public static final String AUTOMATION_NAME = PropertyHelper.getAppiumProperty("appium.automationName");
    public static final String APP_PATH = PropertyHelper.getSerenityProperty("appium.app");
    public static final String APP_PACKAGE = PropertyHelper.getSerenityProperty("appium.appPackage");
    public static final String APP_ACTIVITY = PropertyHelper.getSerenityProperty("appium.appActivity");
    public static final String APPIUM_HUB = PropertyHelper.getSerenityProperty("appium.hub");

}
