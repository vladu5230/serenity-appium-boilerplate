package com.tremend.serenity.utils.constants.sessionvars;

import java.util.Date;

public class SessionVars {

    public static final Date TEST_STARTING_TIME = null;
    public static final String BOOKING_REQUEST_BODY = "bookingRequestBody";
    public static final String BOOKING_RESPONSE_BODY = "bookingResponseBody";
}
