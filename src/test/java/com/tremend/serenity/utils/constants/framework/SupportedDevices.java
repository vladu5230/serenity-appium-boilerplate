package com.tremend.serenity.utils.constants.framework;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum SupportedDevices {

    ANDROID_EMULATOR("androidEmulator"),
    IOS_EMULATOR("iosEmulator");

    @Getter
    private final String value;

    public static SupportedDevices nameOf(String value){
        return Arrays.stream(SupportedDevices.values())
                .filter(e -> e.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", value)));
    }
}
