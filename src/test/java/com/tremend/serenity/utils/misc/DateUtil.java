package com.tremend.serenity.utils.misc;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    private DateUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static LocalDate stringToLocalDate(String date, String format) {
        //-----*   Format examples: yyyy-MM-dd, dd-MM-yy, MM/dd/yyyy, dd-MMM-yyyy  *-----
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
    }

    public static String getCurrentTime(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now); //2016/11/16 12:08:43
    }

    public static long getNowTimeInEpochSecondFormat(){
        Instant instant = Instant.now();
        return instant.getEpochSecond();
    }

    public static long getNowTimeInEpochMilliSecondFormat(){
        Instant instant = Instant.now();
        return  instant.toEpochMilli();
    }

    public static String getNowTimeInEpochMiliSecondString(){
        Instant instant = Instant.now();
        return  String.valueOf(instant.toEpochMilli());
    }
}
