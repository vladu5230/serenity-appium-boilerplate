package com.tremend.serenity.config.api.config;

import com.google.gson.JsonObject;
import com.tremend.serenity.utils.constants.framework.TestConstant;
import com.tremend.serenity.utils.properties.PropertyHelper;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.http.params.CoreConnectionPNames;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class MicroserviceRequest {

    private final RequestSpecification requestSpecification;
    private String basePath = "";
    private static final List<Integer> STATUS_CODES_OK = Arrays.asList(HttpStatus.SC_OK, HttpStatus.SC_CREATED);
    private static final int REST_ASSURED_TIMEOUT = Integer.parseInt(PropertyHelper.getRestAssuredTimeout());

    public MicroserviceRequest(MicroserviceEnum service) {

        this.requestSpecification = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Accept", "application/json");
        setBaseUri(PropertyHelper.getWebDriverBaseUrl());

        addToBasePath(service.getValue());
        if (!TestConstant.AUTH_TOKEN.contains("null")) {
            addHeader("Authorization", "Bearer " + TestConstant.AUTH_TOKEN);
        }

        this.requestSpecification.config(RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, REST_ASSURED_TIMEOUT)
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, REST_ASSURED_TIMEOUT))).relaxedHTTPSValidation();
    }

    public MicroserviceRequest() {

        this.requestSpecification = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Accept", "application/json");
        setBaseUri(PropertyHelper.getWebDriverBaseUrl());

        if (!TestConstant.AUTH_TOKEN.contains("null")) {
            addHeader("Authorization", "Bearer " + TestConstant.AUTH_TOKEN);
        }

        this.requestSpecification.config(RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, REST_ASSURED_TIMEOUT)
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, REST_ASSURED_TIMEOUT))).relaxedHTTPSValidation();
    }

    public void setBaseUri(String baseUri) {
        this.requestSpecification.baseUri(baseUri);
    }

    public void addToBasePath(String pathSegment) {
        this.basePath = this.basePath + "/" + pathSegment;
        this.requestSpecification.basePath(this.basePath);
    }

    public void addHeader(String name, String value) {
        this.requestSpecification.header(name, value);
    }

    public void addCookie(Cookie cookie) {
        this.requestSpecification.cookie(cookie);
    }

    public void addCookie(String cookie) {
        this.requestSpecification.header("Cookie", cookie);
    }

    public void addMultiPartFormData() {
        this.requestSpecification.contentType("multipart/form-data");
    }

    public void addQueryParam(String name, String value) {
        this.requestSpecification.param(name, value);
    }

    public void addBody(String body) {
        this.requestSpecification.body(body);
    }

    public void addBody(File body) {
        this.requestSpecification.body(body);
    }

    public void addBody(JsonObject body) {
        this.requestSpecification.body(body);
    }


    public String getCurrentRequestUrl() {
        return ((RequestSpecificationImpl) this.requestSpecification).getBaseUri() + ((RequestSpecificationImpl) this.requestSpecification).getBasePath();
    }

    public Response executeRequestAndExtractResponse(HttpMethod method) {
        switch (method) {
            case GET:
                return executeGetRequestAndExtractResponse();
            case POST:
                return executePostRequestAndExtractResponse();
            case PUT:
                return executePutRequestAndExtractResponse();
            case DELETE:
                return executeDeleteRequestAndExtractResponse();
            default:
                throw new IllegalStateException(method + " method not defined in MicroserviceRequest.java");
        }

    }

    private Response executeGetRequestAndExtractResponse() {
        Response response;
        if (log.isDebugEnabled()) {
            response = RestAssured.given().spec(this.requestSpecification).when().log().all().get().then().log().all().extract().response();
        } else {
            response = RestAssured.given().spec(this.requestSpecification).when().get().then().extract().response();
            if (!STATUS_CODES_OK.contains(response.getStatusCode()))
                log.error("Status code {} for GET {}", response.getStatusCode(), getCurrentRequestUrl());
        }
        return response;
    }

    private Response executePostRequestAndExtractResponse() {
        Response response;
        if (log.isDebugEnabled()) {
            response = RestAssured.given().spec(this.requestSpecification).when().log().all().post().then().log().all().extract().response();
        } else {
            response = RestAssured.given().spec(this.requestSpecification).when().post().then().extract().response();
            if (!STATUS_CODES_OK.contains(response.getStatusCode()))
                log.error("Status code {} for POST {}", response.getStatusCode(), getCurrentRequestUrl());
        }
        return response;
    }

    private Response executePutRequestAndExtractResponse() {
        Response response;
        if (log.isDebugEnabled()) {
            response = RestAssured.given().spec(this.requestSpecification).when().log().all().put().then().log().all().extract().response();
        } else {
            response = RestAssured.given().spec(this.requestSpecification).when().put().then().extract().response();
            if (!STATUS_CODES_OK.contains(response.getStatusCode()))
                log.error("Status code {} for PUT {}", response.getStatusCode(), getCurrentRequestUrl());
        }
        return response;
    }

    private Response executeDeleteRequestAndExtractResponse() {
        Response response;
        if (log.isDebugEnabled()) {
            response = RestAssured.given().spec(this.requestSpecification).when().log().all().delete().then().log().all().extract().response();
        } else {
            response = RestAssured.given().spec(this.requestSpecification).when().delete().then().extract().response();
            if (!STATUS_CODES_OK.contains(response.getStatusCode()))
                log.error("Status code {} for DELETE {}", response.getStatusCode(), getCurrentRequestUrl());
        }
        return response;
    }
}
