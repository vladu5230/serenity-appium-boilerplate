package com.tremend.serenity.config.api.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MicroserviceEnum {
/**
*   Enum used for Microservice requests. Configure these based on the architecture of your server-side application. Example:
*/
    USER_SERVICE("user-service/api/v1"),
    BOOKING("booking");

    @Getter
    private final String value;
}
