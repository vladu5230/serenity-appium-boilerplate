package com.tremend.serenity.config;

import com.tremend.serenity.config.appium.AppiumServerController;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import net.serenitybdd.core.Serenity;
import org.junit.AfterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;

public class Hooks {

    private Logger log = LoggerFactory.getLogger(Hooks.class);

    @Before
    public void beforeLog(Scenario scenario) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("Starting scenario - " + scenario.getName());
        System.out.println("------------------------------------------------------------------------------------------");
    }

    @Before("@mobile")
    public void before(Scenario scenario) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("Starting Appium Server");
        System.out.println("------------------------------------------------------------------------------------------");
        AppiumServerController.startAppiumServer();
    }

    @After("@mobile")
    public void afterScenario(Scenario scenario) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("Stopping Appium Server");
        System.out.println("------------------------------------------------------------------------------------------");
        Serenity.getDriver().quit();
        AppiumServerController.stopAppiumServer();
    }

    @After()
    public void afterLog(Scenario scenario) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("Ending scenario - " + scenario.getName());
        System.out.println("------------------------------------------------------------------------------------------");
    }
}
