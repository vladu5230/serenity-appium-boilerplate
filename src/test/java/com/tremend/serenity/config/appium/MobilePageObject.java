package com.tremend.serenity.config.appium;

import com.google.common.base.Predicate;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

@Slf4j
public class MobilePageObject extends PageObject {

    public MobilePageObject(final WebDriver driver) {

        super(driver, new Predicate<PageObject>() {
            @SneakyThrows
            @Override
            public boolean apply(PageObject page) {
                try {
                    PageFactory.initElements(new AppiumFieldDecorator(((WebDriverFacade) page.getDriver()).getProxiedDriver(), page.getImplicitWaitTimeout())
                            , page);
                    return true;
                } catch (Exception ignore) {
                    log.info("Waiting for emulator to load app...");
                    TimeUnit.SECONDS.sleep(20);
                    PageFactory.initElements(new AppiumFieldDecorator(((WebDriverFacade) page.getDriver()).getProxiedDriver(), page.getImplicitWaitTimeout())
                            , page);
                    return true;
                }
            }
        });
    }
}
