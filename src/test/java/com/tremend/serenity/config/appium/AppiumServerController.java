package com.tremend.serenity.config.appium;


import com.tremend.serenity.utils.properties.PropertyHelper;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.service.local.flags.ServerArgument;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;


@Slf4j
public class AppiumServerController {

    private static AppiumDriverLocalService service;

    static {
        service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                .withIPAddress("127.0.0.1")
                .usingPort(4723)
                .withArgument(new ServerArgument() {
                    public String getArgument() {
                        return "--avd";
                    }
                }, PropertyHelper.getAppiumProperty("emulator.deviceName"))
                .withStartUpTimeOut(160000, TimeUnit.MILLISECONDS)
                .withArgument(GeneralServerFlag.LOG_LEVEL, "error")
                .withAppiumJS(new File(PropertyHelper.getSerenityProperty("appium.path")))
                .withLogFile(new File("target/appium.log")));
    }

    public static void startAppiumServer() {
        try {
            service.start();
            Assert.assertTrue(service.isRunning());
            log.info("Appium server started: " + service.getUrl());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public static void stopAppiumServer() {
        try {
            if (service.isRunning()) {
                service.stop();
                log.info("Appium server stopped");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
