package com.tremend.serenity.config.appium;

import com.tremend.serenity.utils.properties.PropertyHelper;
import com.tremend.serenity.utils.constants.framework.SupportedDevices;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import lombok.SneakyThrows;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

import static com.tremend.serenity.utils.constants.framework.MobileProperty.*;

public class AppiumDriverCustom implements DriverSource {

    @SneakyThrows
    @Override
    public WebDriver newDriver() {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        switch (SupportedDevices.nameOf(PropertyHelper.getMobileOSType())) {
            case ANDROID_EMULATOR:
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
                capabilities.setCapability(MobileCapabilityType.APP, APP_PATH);
                capabilities.setCapability("appium:ignoreHiddenApiPolicyError", true);
                capabilities.setCapability("dontStopAppOnReset", true);
                capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
                capabilities.setCapability(AndroidMobileCapabilityType.AVD_LAUNCH_TIMEOUT, 160000);
                capabilities.setCapability(AndroidMobileCapabilityType.AVD_READY_TIMEOUT, 160000);
                capabilities.setCapability(AndroidMobileCapabilityType.ADB_EXEC_TIMEOUT, 60000);
                capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_DURATION, 160000);
                capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
                capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
                capabilities.setCapability("chromedriverDisableBuildCheck", true);
                return new AndroidDriver<MobileElement>(new URL(APPIUM_HUB), capabilities);
            case IOS_EMULATOR:
                capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
                capabilities.setCapability(MobileCapabilityType.APP, APP_PATH);
                capabilities.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 60000);
                capabilities.setCapability(MobileCapabilityType.UDID, UDID);
                return new IOSDriver<MobileElement>(new URL(APPIUM_HUB), capabilities);
        }
        return null;
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}

